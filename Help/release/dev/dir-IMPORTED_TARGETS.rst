dir-IMPORTED_TARGETS
--------------------

* The :prop_dir:`IMPORTED_TARGETS` directory property was added to
  get a list of :ref:`Imported Targets` created in the current
  directory.
